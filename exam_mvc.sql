-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 08 mars 2024 à 17:05
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `exam_mvc`
--

-- --------------------------------------------------------

--
-- Structure de la table `creneau`
--

CREATE TABLE `creneau` (
  `id` int(11) NOT NULL,
  `id_salle` int(11) DEFAULT NULL,
  `start_at` datetime NOT NULL,
  `nbrehours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `creneau`
--

INSERT INTO `creneau` (`id`, `id_salle`, `start_at`, `nbrehours`) VALUES
(1, 5, '2024-03-16 00:00:00', 1),
(2, 5, '2024-03-29 00:00:00', 1),
(3, 7, '2024-03-08 09:00:00', 2),
(4, 6, '2024-03-10 14:30:00', 3),
(5, 8, '2024-03-12 11:00:00', 1),
(6, 4, '2024-03-15 10:00:00', 2),
(7, 5, '2024-03-18 15:00:00', 1),
(8, 6, '2024-03-20 08:30:00', 4),
(9, 7, '2024-03-22 12:45:00', 2),
(10, 8, '2024-03-25 09:30:00', 3),
(11, 9, '2024-03-28 13:15:00', 2),
(12, 10, '2024-03-31 16:00:00', 1),
(13, 4, '2024-03-29 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `creneau_user`
--

CREATE TABLE `creneau_user` (
  `id` int(11) NOT NULL,
  `id_creneau` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `creneau_user`
--

INSERT INTO `creneau_user` (`id`, `id_creneau`, `id_user`, `created_at`) VALUES
(1, 1, 1, '2024-03-08 10:30:00'),
(2, 2, 2, '2024-03-10 15:45:00'),
(3, 3, 3, '2024-03-12 11:30:00'),
(4, 4, 4, '2024-03-15 11:30:00'),
(5, 5, 5, '2024-03-18 16:45:00'),
(6, 6, 6, '2024-03-20 09:45:00'),
(7, 7, 7, '2024-03-22 13:00:00'),
(8, 8, 8, '2024-03-25 10:45:00'),
(9, 9, 9, '2024-03-28 14:15:00'),
(10, 10, 10, '2024-03-31 16:45:00'),
(11, 2, 1, '0000-00-00 00:00:00'),
(12, 4, 4, '0000-00-00 00:00:00'),
(13, 2, 7, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `maxuser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `title`, `maxuser`) VALUES
(4, 'Salle A', 10),
(5, 'Salle B', 15),
(6, 'Salle C', 20),
(7, 'Salle D', 12),
(8, 'Salle E', 18),
(9, 'Salle F', 25),
(10, 'Salle G', 8),
(11, 'Salle H', 30),
(12, 'Salle I', 15),
(13, 'Salle J', 22),
(14, 'Salle Test', 500),
(15, 'Salle Test', 500);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `email`) VALUES
(1, 'francoooo', 'franco@mail.co'),
(2, 'test', 'bileldounar@gmail.com'),
(3, 'vfvvfveevc', 'thismas@gmail.c'),
(4, 'John Doe', 'john.doe@example.com'),
(5, 'Jane Smith', 'jane.smith@example.com'),
(6, 'Alice Johnson', 'alice.johnson@example.com'),
(7, 'Bob Williams', 'bob.williams@example.com'),
(8, 'Eva Davis', 'eva.davis@example.com'),
(9, 'Michael Brown', 'michael.brown@example.com'),
(10, 'Sophie Wilson', 'sophie.wilson@example.com'),
(11, 'Alex Miller', 'alex.miller@example.com'),
(12, 'Olivia Moore', 'olivia.moore@example.com'),
(13, 'Daniel Lee', 'daniel.lee@example.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `creneau`
--
ALTER TABLE `creneau`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_salle` (`id_salle`);

--
-- Index pour la table `creneau_user`
--
ALTER TABLE `creneau_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_creneau` (`id_creneau`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `creneau`
--
ALTER TABLE `creneau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `creneau_user`
--
ALTER TABLE `creneau_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `creneau`
--
ALTER TABLE `creneau`
  ADD CONSTRAINT `creneau_ibfk_1` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id`);

--
-- Contraintes pour la table `creneau_user`
--
ALTER TABLE `creneau_user`
  ADD CONSTRAINT `creneau_user_ibfk_1` FOREIGN KEY (`id_creneau`) REFERENCES `creneau` (`id`),
  ADD CONSTRAINT `creneau_user_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
