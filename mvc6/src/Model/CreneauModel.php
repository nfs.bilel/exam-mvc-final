<?php

namespace App\Model;

//use Core\App;
use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours) VALUES (?,?,?)",
            array($post['salle'], $post['start_at'], $post['nbrehours'])
        );
    }
    public static function insertUser($post,$id)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO creneau_user (id_creneau, id_user) VALUES (?,?)",
            array($id,$post['user'])
        );
    }

    public static function allCreaneau()
    {
        return App::getDatabase()->prepare(
            "SELECT c.*, s.title as salle_name
        FROM " . self::$table . " as c
        INNER JOIN salle as s ON c.id_salle = s.id
        ORDER BY c.id",
            array(),
            get_called_class()
        );
    }

    public static function creaneauById($id)
    {
        return App::getDatabase()->prepare(
            "SELECT c.*, s.title as salle_name
        FROM " . self::$table . " as c
        INNER JOIN salle as s ON c.id_salle = s.id
        WHERE c.id =?",
            array($id),
            get_called_class(),
            true
        );
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }
    public function getNbrehours()
    {
        return $this->nbrehours;
    }
}
