<?php

namespace App\Service;

use DateTime;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if (!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if (empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if (!empty($text)) {
            $strtext = strlen($text);
            if ($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif ($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if ($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;
    }

    public function nbValid($nb)
    {

        $error = '';
        if (!empty($nb)) {
            if ($nb <= 0) {
                $error = 'Votre nombre doit etre supérieur à 0';
            } elseif ($nb > 500) {
                $error = 'Votre nombre doit etre inférieurs à 500';
            }
        } else {
            $error = 'Renseignez un nombre.';
        }
        return $error;
    }

    public function dateValid($date)
    {
        $error = '';

        if (empty($date)) {
            $error = 'La date ne peut pas être vide.';
        } else {
            $currentDate = new DateTime();
            $providedDate = new DateTime($date);

            if ($providedDate < $currentDate) {
                $error = 'La date doit être après la date d\'aujourd\'hui.';
            }
        }

        return $error;
    }
}
