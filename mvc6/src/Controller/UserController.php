<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;


/**
 *
 */
class UserController extends BaseController
{

    public function add()
    {
        $title_page = 'Ajout d\'utilisateurs';

        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                UserModel::insert($post);
                $this->addFlash('success', 'Utilisateur ajouté avec succes.');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.user.add', array(
            'title_page' => $title_page,
            'form' => $form,
        ));
    }


    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }



    private function validate($v, $post)
    {
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 2, 70);
        $errors['email'] = $v->emailValid($post['email']);
        return $errors;
    }
}
