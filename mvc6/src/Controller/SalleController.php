<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;


/**
 *
 */
class SalleController extends BaseController
{

    public function add()
    {
        
        $title_page = 'Ajout des Salles';
        
        $errors = array();

        if (!empty($_POST['submitted'])) {            
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Salle ajouté avec succes.');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.salle.add', array(
            'title_page' => $title_page,
            'form' => $form,
        ));
    }


    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }



    private function validate($v, $post)
    {
        $errors['titre'] = $v->textValid($post['titre'], 'titre', 2, 70);
        $errors['maxuser'] = $v->nbValid($post['maxuser']);
        return $errors;
    }
}
