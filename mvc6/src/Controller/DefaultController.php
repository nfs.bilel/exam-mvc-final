<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;

/**
 *
 */
class DefaultController extends BaseController
{
    public function index()
    {
        $message = 'Bienvenue ';

        $salles = SalleModel::all();
        $creneaux = CreneauModel::allCreaneau();
        $users = UserModel::all();

        $this->render('app.default.frontpage', array(
            'message' => $message,
            'users' => $users,
            'creneaux' => $creneaux,
            'salles' => $salles,
        ));
    }


    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
