<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;


/**
 *
 */
class CreneauController extends BaseController
{

    public function add()
    {

        $title_page = 'Ajout des Créneaux';

        $errors = array();

        $salles = SalleModel::all();


        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Creneau ajouté avec succes.');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.creneau.add', array(
            'title_page' => $title_page,
            'form' => $form,
            'salles' => $salles,
        ));
    }

    public function single($id)
    {

        $errors = array();
        $creneau = CreneauModel::creaneauById($id);
        $users = UserModel::all();

        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                CreneauModel::insertUser($post,$id);
                $this->addFlash('success', 'Creneau ajouté avec succes.');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.creneau.single', array(
            'form' => $form,

            'creneau' => $creneau,
            'users' => $users,
        ));
    }


    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }



    private function validate($v, $post)
    {
        $errors['user'] = $v->textValid($post['user'], 'user', 1, 10);
        return $errors;
    }
}
