<?php

$routes = array(
    array('home', 'default', 'index'),

    //user
    array('add-user', 'user', 'add'),

    //salle
    array('add-salle', 'salle', 'add'),

    //creneau
    array('add-creneau', 'creneau', 'add'),
    array('single-creneau', 'creneau', 'single', array('id')),


);
