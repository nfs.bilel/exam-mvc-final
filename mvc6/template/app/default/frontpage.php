<h1><?= $message; ?></h1>

<h2>Table Utilisateurs</h2>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Nom</th>
        <th>Email</th>
    </tr>

    <?php foreach ($users as $key => $user) {
        echo '<tr>';
        echo '<td>' . $user->getId() . '</td>';
        echo '<td>' . $user->getNom() . '</td>';
        echo '<td>' . $user->getEmail() . '</td>';
        echo '</tr>';
    } ?>

</table>

<h2>Table des Salles</h2>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Max Users</th>
    </tr>
    <?php foreach ($salles as $key => $salle) {
        echo '<tr>';
        echo '<td>' . $salle->getId() . '</td>';
        echo '<td>' . $salle->getTitle() . '</td>';
        echo '<td>' . $salle->getMaxuser() . '</td>';
        echo '</tr>';
    } ?>
</table>

<h2>Table des Créneaux</h2>

<table border="1">
    <tr>
        <th>ID</th>
        <th>ID Salle</th>
        <th>Start At</th>
        <th>Nombre d'Heures</th>
        <th>Lien</th>
    </tr>
    <?php foreach ($creneaux as $key => $creneau) {
        echo '<tr>';
        echo '<td>' . $creneau->getId() . '</td>';
        echo '<td>' . $creneau->salle_name . '</td>';
        echo '<td>' . (new DateTime($creneau->getStartAt()))->format('d/m/Y') . '</td>';
        echo '<td>' . $creneau->getNbrehours() . '</td>';
        echo '<td><a href="' . $view->path('single-creneau', array('id' => $creneau->getId())) . '">Voir plus</a></td>';
        echo '</tr>';
    } ?>

</table>