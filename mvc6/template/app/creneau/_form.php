<form action="" method="post" novalidate class="wrapform">

    <?php echo $form->label('Nom de la salle:'); ?>
    <?php echo $form->selectEntity('salle', $salles, 'title'); ?>
    <?php echo $form->error('salle'); ?>

    <?php echo $form->label('Commence le:'); ?>
    <?php echo $form->input('start_at','date'); ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('Nombre d\'heure:'); ?>
    <?php echo $form->input('nbrehours', 'number'); ?>
    <?php echo $form->error('nbrehours'); ?>

    <?php echo $form->submit('submitted', $btnText); ?>
</form>