<form action="" method="post" novalidate class="wrapform">

    <?php echo $form->label('Titre:'); ?>
    <?php echo $form->input('titre'); ?>
    <?php echo $form->error('titre'); ?>

    <?php echo $form->label('Nombre de pers. max:'); ?>
    <?php echo $form->input('maxuser', 'number'); ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', $btnText); ?>
</form>